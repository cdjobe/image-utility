#   Right now it converts .png to jpeg and then numerically numbers them.
#   Eventually, it will contain a menu to do other things, like
#   a type of image tagging...probably involves a data base of image objects

#   to do:
#       * file to keep track of duplicates
#       * GUI so everything else has a reason to be made
#       * user specification of function (what do you want to do?)
#
#       * add user directory specification (not very important atm)
#       * image viewer \....\ Can be interchanged.
#       * tag images   /****/ would just have to have image viewed separately. tags would be easier to implement

from PIL import Image
from PIL import ImageChops
from os import listdir
from os.path import isfile, join
import os
import sglobals as sg
import numpy as np
import time

####################################
####################################
''' DHash() -- from blog.iconfinder.com/detecting-duplicate-images-using-python/
            -- make images comparable
'''            

def DHash(image, hash_size = 8):

    # Grayscale and shrink the image in one step.
    image = image.convert('L').resize(
        (hash_size + 1, hash_size),
        Image.ANTIALIAS,
    )

    pixels = list(image.getdata())
    
    # compare adjacent pixels.
    difference = []
    for row in range(hash_size):
        for col in range(hash_size):
            pixel_left = image.getpixel((col, row))
            pixel_right = image.getpixel((col + 1, row))
            difference.append(pixel_left > pixel_right)
        
    # Convert the binary array to a hexadecimal string.
    decimal_value = 0
    hex_string = []
    for index, value in enumerate(difference):
        if value:
            decimal_value += 2**(index % 8)
        if (index % 8) == 7:
            hex_string.append(hex(decimal_value)[2:].rjust(2, '0'))
            decimal_value = 0
            
    return ''.join(hex_string)
    

# ImageExtension()
# Will return TRUE for an image extension or False for anything else
def ImageExtension(extension):

    if extension == ".jpeg" or extension == ".jpg" or extension == ".gif" or extension == ".bmp" or extension == ".png":
        return True
    else:
        return False
    
# GetInfo - Gets file list of working directory. Used to update the variable sg.file_list
def GetInfo():
    
    sg.file_list = [f for f in listdir(sg.dir_path) if isfile(join(sg.dir_path,f))]
    
    sg.num_files = len(sg.file_list)
    print(sg.num_files)
    
    for  i in range (0, sg.num_files-1):
        name, extension = os.path.splitext(sg.file_list[i])
        #print(i, len(sg.file_list))
        #print(i, sg.file_list[i])
        if (ImageExtension(extension)):
            sg.image_file_list.append(sg.file_list[i])
            sg.file_names.append(name)
            #print(i,extension)
            sg.file_extensions.append(extension)
        '''else:
            sg.file_list.remove(sg.file_list[i])'''
    
    for f in sg.image_file_list:
        print(f)
    #for f in sg.file_list:
     #   print(f)
        
    return
# end GetInfo    


# Opens all images and puts them in list
def OpenAllImages(num_files):

    print("Opening Images")
    
    for i in range (0, num_files-1):
        file_path = sg.dir_path + "\\" + sg.image_file_list[i]
        #print(i, num_files)
        #print(file_path)
        sg.open_images.append(Image.open(file_path))
        sg.open_images[i] = DHash(sg.open_images[i])
        #print(sg.open_images[i])
# end OpenAllImages


# Compare open images
def CompareOpenImages(num_files):

    print("Comparing Images")
    duplicate_list = []
    
    for i in range (0, num_files-1):
        for j in range (i+1, num_files-1):
            #print(i, j)
            if (sg.open_images[i] == sg.open_images[j]):
                duplicate_list.append(sg.image_file_list[i])
                duplicate_list.append(sg.image_file_list[j])
                
    print(duplicate_list)

# end CompareOpenImages    


# ConvertPNG - converts png to jpeg
def ConvertPNG(num_files):
    
    print("Converting .PNG")
    for i in range(0, num_files):
        
        if (sg.file_extensions[i] == '.png'):
            
            old_path = sg.dir_path + "\\" + sg.file_names[i] + sg.file_extensions[i]
            print(old_path)
            sg.file_extensions[i] = ".jpg"
            new_path = sg.dir_path + "\\" + sg.file_names[i] + sg.file_extensions[i]
            print(new_path)
            image = Image.open(old_path)

            image.save(old_path, 'JPEG')
            os.rename(old_path, new_path)            
            
            del image

    MakeLists(num_files)

    return
# end ConvertPNG

# RenameFiles - numerically renames .jpg in working directory from 0 up to 1000
def RenameFiles(num_files):
    
    for i in range(0, num_files):        
        if (sg.file_extensions[i] == '.jpeg') or (sg.file_extensions[i] == '.jpg'):
            
            new_name = str(i)
            file_path = sg.dir_path + "\\" + sg.file_names[i] + sg.file_extensions[i]
            
            if (i < 10):
                new_name = "0" + new_name
            if (i < 100):
                new_name = "0" + new_name
            
            os.rename(file_path, sg.dir_path + "\\" + new_name + sg.file_extensions[i])
    
    print("Renamed files")
    
    return
# end RenameFiles

    
def main():
    
    start_time = time.time() # used to determine running time

    # Available Globals:
    # sg.dir_path = 0 
    # sg.file_list[] = 0
    # sg.file_names[] = 0
    # sg.file_extensions[] = 0
    
    #sg.dir_path = 

    GetInfo()

    sg.num_files = len(sg.file_list)
    print("Number of files = ", sg.num_files)
    
    sg.num_files = len(sg.image_file_list)
    print("Number of files = ", sg.num_files)
    
    #RenameFiles(sg.num_files)   
    OpenAllImages(sg.num_files)
    CompareOpenImages(sg.num_files)
    #CompareImages(num_files)
    
    # Print the running time
    print("--- %d seconds --- " % (time.time() - start_time))
    
    #ConvertPNG(num_files)   
############
main()
